# stop-motion-RPi

Solution alternative au tuto de la fondation Raspberry : https://projects.raspberrypi.org/en/projects/push-button-stop-motion

--> Si impossible de quitter l'interface de la PiCamera ce script réalise la même chose mais ajoute la fonctionnalité d'appuyer longuement sur le bouton pour sortir du programme.